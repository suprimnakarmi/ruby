require "mysql2"

puts "Welcome to the todo app\n\n"
#declare todo array as a global varaible
$todo=Array.new()

def use_database
    begin
        @db=Mysql2::Client.new(host:'localhost',username:'suprim',password: 'P@ssword19',database:'TODOS')
        puts "connected to the MYSQL database server."
        end
            
    rescue Mysql2::Error => e
        puts "error"
        puts "can't connect to the MySQL database specified"
       puts e.message
        exit 1
    
    end
   
    
#function to get input from the user
def choice_from_user
    
    puts("choices available")
    puts("-------------------")
    puts("1. list all todos ")
    puts("2. add new item in todo")
    puts("3. remove item from todo")
    puts("4. update item from todo")
    puts("5. exit")
    puts("------------------------")
    puts ("enter your choice")
    choice=gets.chomp.to_i
    operation(choice)
end


#Operation to perform from the choice given by the user
  def operation(choice) 
    case (choice)
    when 1
        list_all_todo
    when 2
        add_new_item
    when 3
        remove_item_todo
    when 4
        update_item_todo
    when 5
         puts "are you sure you want to exit"
         exit_input=gets.chomp.upcase
        if exit_input.match(/Y\A/) #use of regex to select only the first letter entered by user
        exit
        end  
      

        else
        puts "wrong input"
        puts "--------------"
        choice_from_user
        
    end
end

#function to display all the todo list
def list_all_todo  
  
    results=@db.query("SELECT * FROM task;")
    # serial_no=results.size
    if results.size==0
     puts "\n\n=> your list is empty\n\n"  #print if the list is empty
     choice_from_user
   else      
    puts "\n\n=> the todo list you have are:-"  #print all the content of the list
   results.each_with_index do |no_of_item,index|

    # puts r
     puts "|#{index+1} = #{no_of_item['item']}|"
    
end
    puts("--------------\n\n")
    choice_from_user
  end 
end

#function to add new items to the list
def add_new_item
    puts("what do you want to add?")
    input_value=gets.chomp.downcase    #converting to lower case for efficient comparision
    @db.query("INSERT INTO task(SN,item) VALUES(null,'#{input_value}')")
    puts "\n\n => item added\n\n"
    choice_from_user
    end

#function to remove the items of todo list
def remove_item_todo
    results=@db.query('SELECT item FROM task')
    if results.size==0
        puts("\n\n=>your list is empty\n\n")
        choice_from_user
    else
        puts("enter what you want to delete?") #get direct input from user for what is to be removed 
        del=gets.chomp.downcase
        results.each do |item_name|
             if item_name["item"].eql?(del)
                # puts 2
                @db.query("DELETE FROM task WHERE item='#{del}';")
                puts ("\n\n=> item deleted\n\n")
                choice_from_user
             end
            end
             puts ("\n\n=>Item not found\n\n")  #print if the input doesn't match the content of list
            choice_from_user     
end
end

#update items of todo list
def update_item_todo
results=@db.query("SELECT item FROM task;")
if results.size==0
    puts("\n\n=>the list is empty\n\n")
    choice_from_user
else
    puts("what do you want to update?") #get direct input from user for what is to be updated 
    update_value=gets.chomp.downcase    
       results.each do |check| #loop the rows of the database to check if the value is present or not
        if check["item"].eql?(update_value)
        #   puts 2
            puts("what do you want to update with?")
            new_item=gets.chomp.downcase     #get the item you want to update with
            @db.query("UPDATE task SET item='#{new_item}' WHERE item='#{update_value}'")  #update item by comparing
            puts("\n\n=>the item is updated\n\n")
            choice_from_user
           end
        end
            puts("\n\n=>the item you want to update is not present\n\n")
            choice_from_user
    
    end            
end


use_database
choice_from_user #function call to get input from the user