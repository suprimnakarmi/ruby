

class Players
    @@board=[" "," "," "," "," "," "," "," "," "," ",]

    #method to name the players
  def name_players
    @players=Array.new #defined an array to put player's name
    puts "player1 name: "
    @players[0] = gets.chomp
    puts "player2 name: "
    @players[1] = gets.chomp
    assign_mark
	end
	
	#method to assign mark as X or 0
	def assign_mark
		@assign_mark=Array.new #assigning a new array for mark
		@assign_mark[0] ="1"
		until @assign_mark[0] =="X" || @assign_mark[0] =="0" #loop until player1 chooses X or 0
		puts "#{@players[0]}, what do you want to choose X or 0? "
		@assign_mark[0] =gets.chomp.upcase
	
			if @assign_mark[0] =="X" #condition to select a mark for player2
				@assign_mark[1] ="0"
			else
				@assign_mark[1] ="X"
		end
	end
end

#method to decide who goes first
def who_goes_first
	if rand(1)==1
		puts "#{@players[0]}, you go first"
		@mark=@assign_mark[0]
		position_on_board
	else
		puts "#{@players[1]}, you go first"
		@mark=@assign_mark[1]
		position_on_board
	end
end

	#ask the user which position to place the symbol
	def position_on_board
		puts "which position do you want to place your symbol to"
			position=gets.chomp.to_i
		if check_position(position)
			@@board[position]=@mark
			print_board
		else
			puts "the position is already used,try using other position"
			position_on_board
		end
	end

	#check if the position is already used or not
	def check_position(position)
		if @@board[position]==" "
			return true
		else
			return false
		end
	end



  #method to print the board
		def print_board
			system "clear"
			puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
			puts "@   WELCOME TO TIC TAC TOE GAME  @"
			puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
			puts	"\n\nplayer1: #{@players[0]}"
			puts "player2: #{@players[1]}"
        
        puts "#{@@board[1]}    | #{@@board[2]} | #{@@board[3]}"
        puts "     |   |      "
        puts "-----------------"
        puts "#{@@board[4]}    | #{@@board[5]} | #{@@board[6]}"
        puts "     |   |       "
        puts "-----------------"
        puts "#{@@board[7]}    | #{@@board[8]} | #{@@board[9]}"
        puts "     |   |       "
			check_winner
			end
end

obj=Players.new
obj.name_players