class Animal
    def initialize(breed,name)
        @breed=breed
        @name=name
    end

    def bark
        puts "#{@name} woof woof when he barks and he is #{@breed}"
    end

    def tail
        puts "wags tail"
    end
end

jack=Animal.new("dobermann" , "jackey")
gunn=Animal.new("labrador", "cuchuru")


puts jack.respond_to?("talk")
puts jack.respond_to?("tail")

jack.bark
gunn.tail

puts jack.object_id
puts gunn.object_id



