puts "Welcome to the bank app"
Time.now

class Bank
    @@account=Hash.new  #creating a hash for account

		#method to ask user to create, login the account
    def option_to_user 
        puts "choose the action you want to perform"
        puts "---------------------------------------"
        puts "1. Create an account"
				puts "2. log in to account"
				puts "3. exit"
				choice_made=gets.chomp.to_i

				#switch case to select to create account or login
				case choice_made
					when 1
    				create_account
					when 2
						login_account
					when 3
						puts "thank you for using this app, bye bye"
						exit
					else
						puts "you have a wrong input,please try again"
						option_to_user
					end
				end
			
				#method to create a new account
				def create_account
					puts "enter your user name"
					@user=gets.chomp
					if @@account.has_key?(@user)  #compare if the user name already exists
						puts "the user name already exists, enter something else"
						puts "---------------------------"
						puts @@account
						create_account
						
					else
						@@account.store(@user,0) #set a new username with initial amount 0
						puts "thank you #{@user} for opening an account in our bank, account created"
						puts "----------------------------------------------------------------------"
						puts @@account
						option_to_user
						
					end
				end
			
				#method to login to a account
				def login_account
				puts "Enter your user name"
				@account_name=gets.chomp
					if @@account.has_key?(@account_name) #checks if the account name is present or not
						option_for_account #gives user the action to perform in an account
					else
						puts "unmatched username, check the username or create a new one"
						option_to_user 
					end
				end
			
				#method to perform operation in account
				def option_for_account
					puts "you are logged in, choose what you want to do"
					puts "---------------------------------------------"
					puts "1. Show the current amount"
					puts "2. Withdraw money"
					puts "3. Deposit money"
					puts "4. change user name"
					puts "5. exit"
					account_operation=gets.chomp.to_i
					#switch case to select the operation for the user
					case account_operation
					when 1
						show_current_amount
					when 2
						withdraw_money
					when 3
						deposit_money
					when 4
						option_to_user
					when 5
						exit
					else
						puts "You clicked a wrong number, please check and try again"
						option_for_account
					end
				end
				
				#method to show current amount
				def show_current_amount
					puts "you currently have Rs. #{@@account[@account_name]}"
					option_for_account
				end
			
				#method to withdraw money
				def withdraw_money
				puts "how much money do you want to withdraw?"
				money=gets.chomp.to_i
					if money>@@account[@account_name]   #compare the withdraw money with the account money
						puts "You don't have that amount to withdraw"
						puts "you currently have Rs. #{@@account[@account_name]}"
						option_for_account
					else
						@@account[@account_name] -= money #deduct amount of money from the account after a withdraw
						puts "you withdraw Rs.#{money} , now you have Rs.#{@@account[@account_name]} in your account"
						option_for_account
					end
				end

				#method to deposit money
				def deposit_money
					puts "how much money do you want to deposit?"
					deposit=gets.chomp.to_i
						@@account[@account_name] += deposit #add the deposited amount with account's money
						puts "Rs. #{deposit} was deposited, your current amount is #{@@account[@account_name]}"
						option_for_account
				end
		
		
			end
				obj=Bank.new
				obj.option_to_user
