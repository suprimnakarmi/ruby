class Animal
attr_reader :species, :gender
    def initialize(species,gender)
        @species=species
        @gender=gender
    end

    def food
        puts "Hi, I am #{@species} and i get food"
    end

    def bark
        puts "I dont woof woof"
    end
end

class Dog< Animal
attr_reader :breed, :name
    def initialize(breed,name,gender,species)
        super(species,gender)
        @breed=breed
        @name=name
    end
    
        def bark
            puts"woof woof #{breed}"
        end
end

    
p=Dog.new("labrador","broda","male","dog")
p.bark
puts p.breed
puts p.gender