# Expense Tracker

## About
The Expense tracker is a CLI(Command Line Interface) app which is used to track and record the expenses that are being made. It was completed using Ruby as a programming language.

## Description
The project was completed using following technologies:-

### 1.Ruby
Ruby is a dynamic, open source programming language with a focus on simplicity and productivity. It has an elegant syntax that is natural to read and easy to write. The project was completed using ruby version 2.6.3 .

### 2. Visual Studio Code(VS code)
Visual Studio Code is a source-code editor developed by Microsoft for Windows, Linux and macOS. VS code was used all the way from start to the end to write the code.

### 3. Gems used
Gems like Mysql2 and binding pry were used for the completion of the project.

### 4.Rspec
Rspec is a 'Domain Specific Language'(DSL) testing tool written in Ruby to test Ruby code.It is a behavior-driven development (BDD) framework which is extensively used in production applications. 

## Features(Choices in the main menu)
### 1. Add your income
User can add income anytime.
### 2. Add expense to the record
User can add the expense of each item by selecting the item.
### 3. Show expense table
User can view the expense table in two different ways. One to display the whole table and other to display expense by selecting items.
### 4. Edit estimated expense
User can assign the estimated expense for each item.
### 5. Show estimated expense
User can view estimated expense for the items
### 6. Delete from expense record
User can delete day to day expense from the record.

## Items for expense included
### 1. Food
### 2. Utilities
### 3. Insurance
### 4. Transportation
### 5. Entertainment
### 6. Taxes
### 7. Miscellaneous
