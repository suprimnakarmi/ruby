require "mysql2"

class Database
  def initialize
    db = Mysql2::Client.new(host: "localhost", username: "suprim", password: "P@ssword19", database: "trackers")
    db.query("SET FOREIGN_KEY_CHECKS = 0;")
    db.query("TRUNCATE TABLE expenses;")
    db.query("TRUNCATE TABLE expense_tracker")
    db.query("SET FOREIGN_KEY_CHECKS =1;")
    db.query("CREATE TABLE IF NOT EXISTS expenses(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,item varchar(100),estimated_expense int, actual_expense int);")
    db.query("CREATE TABLE IF NOT EXISTS expense_tracker(SN INT NOT NULL AUTO_INCREMENT PRIMARY KEY,total_expense INT, date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,item_id INT,FOREIGN KEY(item_id) REFERENCES expenses(id));")
    db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'FOOD',0,0);")
    db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'UTILITIES',0,0);")
    db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'INSURANCE',0,0);")
    db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'TRANSPORTATION',0,0);")
    db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'ENTERTAINMENT',0,0);")
    db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'TAXES',0,0);")
    # @db.query("INSERT INTO expenses(id,item,estimated_expense,actual_expense) VALUES (null,'MISCELLANEOUS',0,0);")

  end

  def self.connection
    Mysql2::Client.new(host: "localhost", username: "suprim", password: "P@ssword19", database: "trackers")
  end
end
