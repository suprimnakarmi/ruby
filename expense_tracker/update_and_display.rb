require "pry"

class Update_and_display
  def initialize(con)
    @db = con
  end

  #method to display estimated expense and actual expense
  def show_estimated_expense
    system "clear"
    results = @db.query("SELECT * FROM expenses;")
    puts "|ID | ITEMS      ||  ESTIMATED AMOUNT   | ACTUAL AMOUNT  "
    results.each do |show|
      puts "----------------------------------------"
      puts "|#{show["id"]} | #{show["item"]}" + "   |" + "|#{show["estimated_expense"]} | #{show["actual_expense"]}"
    end
    puts "----------------------------------------"
  end

  #method to show options for user to display selected item
  def show_selected_items
    puts "Select which item's expense list to select?"
    puts "-------------------------------------------"
    puts "1. FOOD"
    puts "2. UTILITIES"
    puts "3. INSURANCE"
    puts "4. TRANSPORTATION"
    puts "5. ENTERTAINMENT"
    puts "6. TAXES"
    # puts "7. MISCELLANEOUS"
    puts "7. Go back to top menu"
    select_items = gets.chomp.to_i #get choice from user to display
    display_selected_item(select_items)
  end

  #method to display the expense of selected items(called from show_selected_items)
  def display_selected_item(select_items)
    case select_items
    when 1
      actual_expense_display("FOOD") #calling method to display date and price
    when 2
      actual_expense_display("UTILITIES")
    when 3
      actual_expense_display("INSURANCE")
    when 4
      actual_expense_display("TRANSPORTATION")
    when 5
      actual_expense_display("ENTERTAINMENT")
    when 6
      actual_expense_display("TAXES")
      # when 7
      # actual_expense_display('MISCELLENOUS')
    when 7
      puts "displaying top menu"
    end
  end

  #method called to display date and price of each item(called from income_expense)
  def actual_expense_display(selected_item)
    system "clear"
    records = @db.query("SELECT SN,date,item,total_expense FROM expenses e,expense_tracker t WHERE e.id=t.item_id AND item='#{selected_item}'; ")
    puts "SN   | DATE/TIME                  | ITEM | TOTAL_EXPENSE"
    records.each do |show|
      puts "|--------------------------------------------------|"
      puts "|#{show["SN"]}   #{show["date"]}" + "   |" + "|#{show["item"]} | #{show["total_expense"]}|"
    end
    puts "|---------------------------------------------------|"
  end

  #display the table to let user delete the selected item(called from income_expense)
  def display_to_delete
    records = records = @db.query("SELECT SN,date,item,total_expense FROM expenses e,expense_tracker t WHERE e.id=t.item_id")
    records.each do |show|
      puts "|--------------------------------------------------|"
      puts "|#{show["SN"]} | #{show["date"]}" + "   |" + "|#{show["item"]} | #{show["total_expense"]}|"
    end
    puts "|---------------------------------------------------|"
  end

  #method to update estimated expense in the table(called from income_expense)
  def update_estimated_expense(item_cost, select_item)
    @db.query("UPDATE expenses SET estimated_expense=#{item_cost} WHERE item='#{select_item}';")
  end

  #method called everytime the actual_money is to be updated(called from income_expense)
  def actual_update_data(selected_item, item_expense)
    begin
      results = @db.query("SELECT id,actual_expense FROM expenses WHERE item='#{selected_item}';")
      actual_money = 0    #value
      actual_id = 0
      results.each do |show| #loop to access the entries of database
        actual_id = show["id"]
        actual_money = show["actual_expense"]
      end
      @db.query("INSERT INTO expense_tracker (SN,total_expense,date,item_id) VALUES (null,#{item_expense},null,#{actual_id});")
      item_expense += actual_money
      @db.query("UPDATE expenses SET actual_expense=#{item_expense} WHERE id=#{actual_id};")
    rescue => e
      e.message
      puts "database error"
    end
  end
end
