require "./database.rb"
require "pry"
require "./income_expense.rb"
require "./update_and_display.rb"
require "./compare_and_delete.rb"

#class to run
class Expense_tracker
  def initialize(dbconnection)
    @db = dbconnection    #intializing the object of database
    con = @db
    @update_object = Update_and_display.new(con)
    @compare_object = Compare_and_delete.new(con, @update_object)
    @income_expense = Income_expense.new(con, @update_object, @compare_object)
  end

  def main_display
    puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    puts "@   EXPENSE TRACKER APP    @"
    puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n"
  end

  #method to provide user with options to select
  def option_for_user
    puts "||======================||"
    puts "||      MAIN MENU       ||"
    puts "||======================||\n"
    puts "1. Add your income"
    puts "2. Add expense to the record"
    puts "3. Show expense table"
    puts "4. Edit estimated expense"
    puts "5. Show estimated expense"
    puts "6. Delete from expense record "
    puts "7. Exit"
    puts "-------------------------------"
    puts "which option do you want to select?"
    choose_option = gets.chomp.to_i
    choice_of_user(choose_option)  #call the method to perform the action
  end

  #method to perform the action
  def choice_of_user(choose_option)
    case choose_option
    when 1
      @compare_object.add_income    #calling method from the class Income_expense
      option_for_user
    when 2
      @income_expense.add_expenses  #called in income_expense
      option_for_user
    when 3
      @income_expense.show_expenses  #calling method to show expense
      option_for_user
    when 4
      @income_expense.add_estimated_expense  #calling method to estimate expense
      option_for_user
    when 5
      @update_object.show_estimated_expense #calling method to estimate expense
      option_for_user
    when 6
      @income_expense.delete_expense  #calling method to delete expense
      option_for_user
    when 7
      puts "\n\n ****************THANKS FOR USING*********************"
      exit
    else
      puts "wrong input,please try again"
      option_for_user
    end
  end
end

dbconnection = Database.connection #calling method of database
Database.new
obj_expense = Expense_tracker.new(dbconnection)
obj_expense.main_display
obj_expense.option_for_user
