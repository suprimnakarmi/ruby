require "./update_and_display.rb"
require "pry"

#class to manipulate all the values of database
class Income_expense
  def initialize(con, obj_update, obj_compare)
    @db = con                 #initializing object of the database
    @update_object = obj_update
    @compare_object = obj_compare
    @income = 0
    @compare_money = 0
    @food, @utilities, @insurance, @transportation, @entertainment, @taxes = "a", "b", "c", "d", "e", "f"
  end

  #add estimated expense in the table(called from expense_tracker)
  def add_estimated_expense
    puts "\n\nHow much money do you want to estimate for each?"
    puts "---------------------------------------"

    until @food.match?(/\d/)
      puts "1. FOOD"
      print "Rs."
      @food = gets.chomp    #get input from user (manually add the amount for each item)
    end
    @compare_object.check_estimate_with_income(@food.to_i, "FOOD")

    until @utilities.match?(/\d/)
			puts "2. UTILITIES"  #get estimated value for utilities
			print "Rs."
      @utilities = gets.chomp
    end
    @compare_object.check_estimate_with_income(@utilities.to_i, "UTILITIES")

    until @insurance.match?(/\d/)
			puts "3. INSURANCE"  #get estimated value for insurance
			print "Rs."
      @insurance = gets.chomp
    end
    @compare_object.check_estimate_with_income(@insurance.to_i, "INSURANCE")

    until @transportation.match?(/\d/)
			puts "4. TRANSPORTATION" #get estimated value for transportation
			print "Rs."
      @transportation = gets.chomp
    end
    @compare_object.check_estimate_with_income(@transportation.to_i, "TRANSPORTATION")

    until @entertainment.match?(/\d/)
			puts "5. ENTERTAINMENT"   #get estimated value for entertainment
			print "Rs."
      @entertainment = gets.chomp
    end
    @compare_object.check_estimate_with_income(@entertainment.to_i, "ENTERTAINMENT")

    until @taxes.match?(/\d/)
			puts "6. TAXES"  #get estimated value for taxes
			print "Rs."
      @taxes = gets.chomp
    end
    @compare_object.check_estimate_with_income(@taxes.to_i, "TAXES")

    # until @miscellaneous.match?(/\d/)
    # puts "7. MISCELLANEOUS"   #get estimated value for
    # @miscellaneous=gets.chomp
    # end
    #   @compare_object.check_estimate_with_income(@miscellaneous.to_i,'MISCELLANEOUS')
    system "clear"
  end

  #add expense to the table(called from expense_tracker)
  def add_expenses
    puts "||=======================||"
    puts "||  MENU TO ADD EXPENSE  ||"
    puts "||=======================||"
    puts "Add expense to which item?"
    puts "---------------------------------------"
    print "1. FOOD\n2. UTILITIES\n3. INSURANCE\n4. TRANSPORTATION\n5. ENTERTAINMENT\n6. TAXES\n7. Go back\n\n"
    item_choice = gets.chomp.to_i
    actual_expense_added(item_choice)
  end

  #add actual spent money to the table(called from add_expense)
  def actual_expense_added(item_choice)
    case item_choice
    when 1
      puts "How much did you spend in food?"
      ex_food = gets.chomp
      if ex_food.match?(/\d/)
        @compare_object.check_with_income(ex_food.to_i, "FOOD")
        @compare_object.compare_with_estimate(@food.to_i, ex_food.to_i, "FOOD")
      else
        puts "Error in input.. try again"
        add_expenses
      end
    when 2
      puts "How much did you spend in utilities?"
      ex_utilities = gets.chomp
      if ex_utilities.match?(/\d/)
        @compare_object.check_with_income(ex_utilities.to_i, "UTILITIES")
        @compare_object.compare_with_estimate(@utilities.to_i, ex_utilities.to_i, "UTILITIES")
      else
        puts "Error in input.. try again"
        add_expenses
      end
    when 3
      puts "How much did you spend in insurance?"
      ex_insurance = gets.chomp
      if ex_insurance.match?(/\d/)
        @compare_object.check_with_income(ex_insurance.to_i, "INSURANCE")
        @compare_object.compare_with_estimate(@insurance.to_i, ex_insurance.to_i, "INSURANCE")
      else
        puts "Error in input.. try again"
        add_expenses
      end
    when 4
      puts "How much did you spend in transportation?"
      ex_transportation = gets.chomp
      if ex_transportation.match?(/\d/)
        @compare_object.check_with_income(ex_transportation.to_i, "TRANSPORTATION")
        @compare_object.compare_with_estimate(@transportation.to_i, ex_transportation.to_i, "TRANSPORTATION")
      else
        puts "Error in input.. try again"
        add_expenses
      end
    when 5
      puts "How much did you spend in entertainment?"
      ex_entertainment = gets.chomp
      if ex_entertainment.match?(/\d/)
        @compare_object.check_with_income(ex_entertainment.to_i, "ENTERTAINMENT")
        @compare_object.compare_with_estimate(@entertainment.to_i, ex_entertainment.to_i, "ENTERTAINMENT")
      else
        puts "Error in input.. try again"
        add_expenses
      end
    when 6
      puts "How much did you spend in taxes?"
      ex_taxes = gets.chomp
      if ex_taxes.match?(/\d/)
        @compare_object.check_with_income(ex_taxes.to_i, "TAXES")
        @compare_object.compare_with_estimate(@taxes.to_i, ex_taxes.to_i, "TAXES")
      else
        puts "Error in input.. try again"
        add_expenses
      end

      # when 7
      #   puts "How much did you spend in miscellanous?"
      #   ex_miscellanous= gets.chomp
      #   if ex_miscellanous.match?(/\d/)
      #   @compare_object.check_with_income(ex_miscellanous.to_i,'MISCELLANOUS')
      #   @compare_object.compare_with_estimate(@miscellanous.to_i,ex_miscellanous.to_i,'miscellanous')
      #   else
      #   puts "Error in input.. try again"
      #   add_expenses
      # end

    when 7
      puts "exiting!!"
    else
      puts "wrong input,please enter the valid input"
      add_expenses
    end
  end

  #method to display the expense table(called from expense_tracker)
  def show_expenses
    system "clear"
    puts "||=========================||"
    puts "||     DISPLAY OPTION      ||"
    puts "||=========================||"
    puts "How do you want to see the table?"
    puts "---------------------------------"
    puts "1. Display table with estimated and actual expense"
    puts "2. Display table with detail of expense of certain item"
    puts "3. Go back"
    display_choice = gets.chomp.to_i
    option_to_display(display_choice)
  end

  #method to operate on choice of display by the user(called from show_expenses)
  def option_to_display(display_choice)
    case display_choice
    when 1
      @update_object.show_estimated_expense    #call method to display expense table
    when 2
      @update_object.show_selected_items
    when 3
      puts "Displaying top menu"
    else
      "wrong input,please try again"
      show_expense_table
    end
  end

  #let user choose what he/she wants to delete
  def delete_expense
    @update_object.display_to_delete
    puts "===================================================="
    puts "select SN of item you want to delete from?"
    delete_item = gets.chomp.to_i
    @compare_object.delete_from_table(delete_item)
  end
end
