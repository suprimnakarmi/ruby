class Compare_and_delete
  def initialize(con, obj_update)
    @db = con
    @update_object = obj_update
    @income = 0
    @compare_money = 0
  end

  #call this method if user don't have enough money to estimate sdfl
  def not_enough_money
    begin
      puts "\n\n==>You don't have enough money"
      puts "\n\n==>Do you want to add income?"
      check = gets.chomp.downcase
      if check == "y"
        add_income
      else
        puts "Warning!! you cannot add any more items,you should add money first.\n\n"
      end
    rescue => e
      e.message
      puts "your income cannot be added.. try again later"
    end
  end

  #method to add income(called from expense_tracker and not_enough_money)
  def add_income
    print "add your income: "
    money = gets.chomp.to_i
    @income += money
    system "clear"
    puts "\n\n==> The total money you have is Rs.#{@income}\n\n"
    @compare_money = @income   #assign a variable to compare from estimated money
  end

  #method to check if income is enough to spend or not(called from actual_expense_added)
  def check_with_income(selected_item, item_in_table)
    if selected_item > @income
      not_enough_money  #method to add money in income
    end
    if selected_item < @income
      @update_object.actual_update_data(item_in_table, selected_item)
      @income -= selected_item
      puts ("\n\n ==> you spent Rs.#{selected_item} in #{item_in_table} and you have Rs.#{@income} remaining. Have a good day :)")
    end
  end

  #method to check the estimated expense with the income(called from income_expense)
  def check_estimate_with_income(selected_item, item_in_table)
    if selected_item > @compare_money
      not_enough_money
    end
    @compare_money -= selected_item
    @update_object.update_estimated_expense(selected_item, item_in_table)
  end

  #to compare the estimated expense with the actual expense of the item(called from income_expense)
  def compare_with_estimate(estimated_money, actual_money, item)
    begin
      results = @db.query("SELECT actual_expense FROM expenses WHERE item='#{item}' ")
    rescue => e
      puts e.message
      puts "database error!"
    end
    results.each do |show|
      actual_money = show["actual_expense"]
    end
    if actual_money > estimated_money
      puts "==>WARNING!! your actual budget is exceeding the estimated for #{item}"
      puts "----------------------------------------------------------------------"
    else
      puts "----------------------------------------------------------"
    end
    # rescue =>e
    #   puts "It is better to input estimation value first"
    #   puts e.message
    #end

  end

  #to delete the items from the expense tracker table
  def delete_from_table(delete_item)
    #get the entry(money) and id from expense_tracker
    begin
      results = @db.query("SELECT total_expense, item_id FROM expense_tracker WHERE SN=#{delete_item}")
      substract_expense = 0
      id = 0
      results.each do |show|
        substract_expense = show["total_expense"]
        id = show["item_id"]
      end
      #get the actual_money from the expenses table
      records = @db.query("SELECT actual_expense FROM expenses WHERE id=#{id}")
      actual_expense = 0
      records.each do |extract|
        actual_expense = extract["actual_expense"]
      end
      actual_expense -= substract_expense #substract the deleted item to the money of main table
      @db.query("UPDATE expenses SET actual_expense=#{actual_expense} WHERE id=#{id}")
      @db.query("DELETE FROM expense_tracker WHERE SN=#{delete_item}")
    rescue => e
      puts "database error"
      e.message
    end
  end
end
