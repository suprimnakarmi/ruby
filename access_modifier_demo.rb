class Introduction
    attr_accessor :name, :age, :address
   
    def initialize(name,age,address)
        self.name=name
        self.age=age
        self.address=address
    end



    def speak
        puts "hello my name is #{@name} . I am #{@age} years old and I live in #{@address}."

    end


  
    def spider
        private_info
    end

    def private_info
        puts "my name is not #{@name} but it's Alex"
    end
  
    #use private like this    
    private :private_info
    
    #use protected like this
    protected 
    def self.know_me
        puts "my address is not #{@address} but it's sanepa"
    end

  

 
end


n=Introduction.new("suprim", 15 , "jawalakhel")
n.speak
n.spider
Introduction.know_me

